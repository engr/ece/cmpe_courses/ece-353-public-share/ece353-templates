#include "timer.h"

void timer_init(cyhal_timer_t *timer_obj, cyhal_timer_cfg_t *timer_cfg, uint32_t ticks, void *Handler)
{
    cy_rslt_t rslt = CY_RSLT_SUCCESS;

    timer_cfg->compare_value = 0;
    timer_cfg->period = ticks - 1;
    timer_cfg->direction = CYHAL_TIMER_DIR_UP;
    timer_cfg->is_compare = false;
    timer_cfg->is_continuous = true;
    timer_cfg->value = 0;

    /* ADD CODE */
    /* Initialize a timer */
    
    CY_ASSERT(rslt == CY_RSLT_SUCCESS); // If the initialization fails, halt the MCU

    /* ADD CODE */
    /* Apply timer configuration such as period, count direction, run mode, etc. */
   
    CY_ASSERT(rslt == CY_RSLT_SUCCESS); // If the initialization fails, halt the MCU

    /* ADD CODE */
    /* Set the frequency of timer*/
  
    CY_ASSERT(rslt == CY_RSLT_SUCCESS); // If the initialization fails, halt the MCU

    /* ADD CODE */
    /* Assign the ISR to execute on timer interrupt */

    /* ADD CODE */
    /* Set the event on which timer interrupt occurs and enable it */

    /* ADD CODE */
    /* Start the timer with the configured settings */
    
    CY_ASSERT(rslt == CY_RSLT_SUCCESS); // If the initialization fails, halt the MCU   
}